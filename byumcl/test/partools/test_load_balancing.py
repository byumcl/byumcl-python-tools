#test_partools_load_balancing.py

import unittest
import byumcl.partools.load_balancing as lb

#Notice that I have created a seperate class for each function in the load_balancing module. I've done this for organizational purposes. 


class Test_ordered_balance(unittest.TestCase):

    def setUp(self):
        pass

    def test_ordered_balance(self):
        #mutiple test examples
        self.assertEqual((0, 2, 2), lb.ordered_balance(0, 3, 5))
        self.assertEqual((2, 4, 2), lb.ordered_balance(1, 3, 5))
        self.assertEqual((4, 5, 1), lb.ordered_balance(2, 3, 5))
        self.assertEqual((0, 2, 2), lb.ordered_balance(0, 3, 4))
        self.assertEqual((2, 3, 1), lb.ordered_balance(1, 3, 4))
        self.assertEqual((3, 4, 1), lb.ordered_balance(2, 3, 4))

class Test_gatv_scatv_tuples(unittest.TestCase):

    def setUp(self):
        pass

    def test_gatv_scatv_tuples(self):
        #test examples
        gv = lb.gatv_scatv_tuples(3, 5)
        self.assertEqual(gv[0], (2, 2, 1))
        self.assertEqual(gv[1], (0, 2, 4))
        gv = lb.gatv_scatv_tuples(3, 4)
        self.assertEqual(gv[0], (2, 1, 1))
        self.assertEqual(gv[1], (0, 2, 3))

if __name__ == '__main__':
    unittest.main()

