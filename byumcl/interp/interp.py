"""
Created Jan 23, 2013


Author: Chase Coleman and Spencer Lyon

Cubic spline interpolation routines

TODO: Add re-scaling of nodes for more accuracy

TODO: Cythonize

TODO: Make CubicSpline2d call the 1d class, not byumcl.interp.cubicspline1d

TODO: Add nd class

TODO: Maybe we should have a _Spline base class that defines make grid,
      _calcuv_i, and other common methods.
"""
from __future__ import division
import math
import numpy as np
from byumcl.interp.cubicspline1d import calccubicspline1d
import numdifftools as nd
from scipy.misc import derivative as der
import scipy.linalg as la

__all__ = ['CubicSpline2d', 'CubicSpline1d']


def _gridbuild(a, b, n):
    """
    This function takes a,b,n as inputs and will return
    grid and has output
    a : the left endpoint of the interval you want to interpolate over
    b : the right endpoint of the interval you want to interpolate over
    n : the number of equally spaced points you want to use

    grid : an evenly spaced interval in R1 with n+1 nodes
    h : the size of your steps
    """

    grid = np.linspace(a, b, n + 1)
    h = grid[1] - grid[0]
    return grid, h


def _calcuv_i(self, x, x_or_y, i=None):
    """
    This function calculates the value of u_i and v_i by applying
    the function phi to the t   The u_i and v_iare the basis
    functions used to evaluate the spline

    x : Value of x (or y) we are using
    a : left endpoint
    h : step size
    i : which step you are on
    """
    # Get values of a or h depending on whether we are using x or y
    if x_or_y == 'x':
        a = self.lx
        h = self.hx
    elif x_or_y == 'y':
        a = self.ly
        h = self.hy
    else:
        raise ValueError("Variable x_or_y must have value equal to \
                         either 'x' or 'y'")

    #Determine t
    t = ((x - a) / h) + 2. - i

    #Calculate the value of the function phi(t)
    if np.abs(t) <= 1:
        out = 4. - 6. * np.abs(t) ** 2 + 3. * np.abs(t) ** 3
    elif 1 < np.abs(t) <= 2:
        out = (2 - np.abs(t)) ** 3
    else:
        out = 0

    return out


class CubicSpline1d(object):
    """
    This class takes the x and y values to a 1d problem and calculates
    the spline evaluation of the function.

    Parameters
    ----------
    lx : float
        Lower bound of where you are approximating.  Double

    ux : float
        Upper bound of where you are approximating.  Double

    nx : int
        Number of nodes to be used in the spline approximation. Int

    alpha : float
        Value of 2nd deriv @ lx. Double

    beta : float
        Value of 2nd deriv @ ux. Double

    Returns
    -------
    None
    """

    def __init__(self, lx, ux, nx, alpha=0, beta=0):
        self.lx = lx
        self.ux = ux
        self.nx = nx
        self.alpha = alpha
        self.beta = beta

        self.grid, self.hx = _gridbuild(self.lx, self.ux, self.nx)

    def coefs(self, y):
        """
        This function calculates a cubic spline for a 1 dimensional case
        We proceed on the method explained in Habermann and Kindermann 2007
        It should run faster than typical cubic B-Spline interpolation and
        maintain the accuracy.

        Inputs:
        x : Your x values.  Should be evenly spaced.  Use gridbuild func to choose x
        y : Value of f(x) for the f that you are trying to approximate
        alpha : value of 2nd derivative of f at alpha
        beta : value of 2nd derivative of f at beta

        output:
        c : coefficients s.t. s(x) = sum(c_k * u_k(x))
        """
        alpha = self.alpha
        beta = self.beta
        h = self.hx
        a = self.lx
        b = self.ux
        x = self.grid
        y = np.squeeze(y)
        n = x.size - 1   # Python only indexes up to n-1, but there are n terms in x
        # ie the nth term is x[n-1] b/c python include x[0]

        # Check simple errors.  x and y should be same size and x should be evenly spaced
        if x.size != y.size:
            raise ValueError('x and y should be the same size. Cannot proceed')
        if x[2] - 2 * x[1] + x[0] > 1e-6:
            raise ValueError('x should be evenly spaced.  Cannot proceed')

        # Initialize the u matrices
        umat = np.zeros((n + 3, n + 3))
        for row in xrange(1, x.size + 1):
            for column in xrange(umat.shape[1]):
        # for row from 1 <= row < xsize + 1:
        #     for column from 0 <= column < umat.shape[1]:
                umat[row, column] = _calcuv_i(self, x[row - 1], 'x', column + 1)

        umat[0, :3] = [1, -2, 1]
        umat[-1, -3:] = [1, -2, 1]

        # We reduce our umat to a smaller tridiagonal matrix by simple mat algebra
        # We will fulfill conditions for c1,c2,cn+2,cn+3
        lumat = umat[2:-2, 2:-2]

        c1 = 1 / 6 * (y[0] - (alpha * h ** 2) / 6)  # put in c[1]. This is c_{2}
        cm2 = 1 / 6 * (y[-1] - (beta * h ** 2) / 6)  # put in c[-2]. this is c_{n + 2}

        # We have umat * c_n = b
        # To solve this we will first apply simple linear algebra and obtain c2,cn+2
        c_n = np.zeros(n + 3)  # c ranges from 1 to n+3 so we need n+3 spots
        c_n[1] = c1
        c_n[-2] = cm2

        lb = np.zeros(n - 1)
        lb[0] = y[1] - c1
        lb[-1] = y[-2] - cm2
        lb[1:-1] = y[2:-2]

        #We then will solve the middle piece which is lumat * c[middle part] = byvec[middlepart]
        c_n[2:-2] = la.solve(lumat, lb)

        #Then we obtain c_1 and c_n+3 by lin alg
        c_n[0] = ((alpha * h ** 2) / 6.) + 2. * c_n[1] - c_n[2]
        c_n[-1] = ((beta * h ** 2) / 6.) + 2. * c_n[-2] - c_n[-3]

        self.coef = c_n

        return c_n

    def feval(self, x):
        """
        This function takes a value x and the coefficients obtained by calccubicspline and
        evaluates the spline at x.

        Inputs:
        x : The point at which you are evaluating the spline
        a : The left endpoint
        h : The step size obtained by gridbuild
        coeffs : The coefficients obtained in calccubicspline

        Outputs:
        y_spline : The estimated value of y.  (Value of the interpolation s(x).)
        """
        a = self.lx
        h = self.hx
        n = self.nx
        coeffs = self.coef

        l = math.floor((x - a) / h) + 1
        m = np.min([l + 3, n + 3])

        #We want to build a vector of possible u_ks
        uk_vec = np.zeros(m - l + 1)

        for column in xrange(int(m - l + 1)):
            uk_vec[column] = _calcuv_i(self, x, 'x', column + l)

        y_spline = np.dot(uk_vec, coeffs[l - 1:m])

        return y_spline


class CubicSpline2d(object):
    """
    TODO : Have Chase fill in the docstring that describes what the class
    does / references.
    """

    def __init__(self, lx, ly, ux, uy, nx, ny, alpha, beta):
        self.lx = lx
        self.ly = ly
        self.ux = ux
        self.uy = uy
        self.nx = nx
        self.ny = ny
        self.alpha = alpha
        self.beta = beta

        self.make_grid()

    def make_grid(self):
        """
        This function takes a(xy),b(xy),n(xy) as inputs and will return
        grid and has output
        a : the left endpoint of the interval you want to interpolate over.
        b : the right endpoint of the interval you want to interpolate over
        n : the number of equally spaced points you want to use


        grid : an evenly spaced interval in R1 with n+1 nodes
        h : the size of your steps

        x and y denote which variable it refers to for all of the above.

        TODO : Use the _gridbuild function instead.
        """
        xgrid = np.linspace(self.lx, self.ux, self.nx + 1)
        ygrid = np.linspace(self.ly, self.uy, self.ny + 1)
        self.hx = xgrid[1] - xgrid[0]
        self.hy = ygrid[1] - ygrid[0]
        self.xgrid = xgrid
        self.ygrid = ygrid

        return xgrid, ygrid

    def _calcuv_i(self, x, x_or_y, i=None):
        """
        This function calculates the value of u_i and v_i by applying
        the function phi to the t   The u_i and v_iare the basis
        functions used to evaluate the spline

        x : Value of x (or y) we are using
        a : left endpoint
        h : step size
        i : which step you are on
        """
        # Get values of a or h depending on whether we are using x or y
        if x_or_y == 'x':
            a = self.lx
            h = self.hx
        elif x_or_y == 'y':
            a = self.ly
            h = self.hy
        else:
            raise ValueError("Variable x_or_y must have value equal to \
                             either 'x' or 'y'")

        #Determine t
        t = ((x - a) / h) + 2. - i

        #Calculate the value of the function phi(t)
        if np.abs(t) <= 1:
            out = 4. - 6. * np.abs(t) ** 2 + 3. * np.abs(t) ** 3
        elif 1 < np.abs(t) <= 2:
            out = (2 - np.abs(t)) ** 3
        else:
            out = 0

        return out

    def coefs(self, z):
        """
        This function calculates a cubic spline for a 2 dimensional case
        We proceed on the method explained in Habermann and Kindermann 2007
        It should run faster than typical cubic B-Spline interpolation and
        maintain or even improve the accuracy.

        Parameters
        ----------
        object : array_like
        z : array-like, dtype=float
            Value of f(x, y) for the f that you are trying to
            approximate

        Returns
        -------
        c : array_likt, dtype=float
            Coefficients s.t. s(x) = sum(c_ij * u_i(x) * v_j(x))
        """
        x = np.squeeze(self.xgrid)
        y = np.squeeze(self.ygrid)

        nx = self.nx
        ny = self.ny

        # Check simple errors.  x and y should be same size and x should be
        # evenly spaced
        if x.size * y.size != z.size:
            raise ValueError('z should have a size of  nx * ny. Cannot proceed')
        if x[2] - 2 * x[1] + x[0] > 1e-6:
            raise ValueError('x should be evenly spaced.  Cannot proceed')
        if y[2] - 2 * y[1] + y[0] > 1e-6:
            raise ValueError('y should be evenly spaced.  Cannot proceed')
        #Initialize the u matrices

        # Create a cstar matrix.  Then fill it with coefficients calculated by
        # running the 1d cubic spline over every possible y and letting x
        #  change
        cstar = np.zeros((nx + 3, ny + 1))

        for i in xrange(y.size):  # TODO : remove old calccubiccpline1d dep.
            ctemp = calccubicspline1d(x, z[:, i], self.alpha, self.beta,
                                      self.hx)
            cstar[:, i] = ctemp.squeeze()

        # Create a true coeff mat.  Then fill it by running the 1d cubic
        # spline fixed at all of the points in y.

        c_mat = np.zeros((nx + 3, ny + 3))

        for i in xrange(nx + 3):
            ctemp = calccubicspline1d(y, cstar[i, :], self.alpha,
                                      self.beta, self.hy)
            c_mat[i, :] = ctemp

        self.c_mat = c_mat

        return c_mat

    def eval(self, point):
        """
        This function takes a value x and the coefficients obtained by calccubicspline and
        evaluates the spline at x.

        Inputs:
        x,y : The point at which you are evaluating the spline
        a(xy): The left endpoint
        h(xy): The step size obtained by gridbuild2
        coeffs : The coefficients obtained in calccubicspline2

        Outputs:
        z_spline : The estimated value of z.  (Value of the interpolation s(x).)
        """
        def find_z_point(x, y):
            tempsum = 0

            lx = math.floor((x - self.lx) / self.hx) + 1
            ly = math.floor((y - self.ly) / self.hy) + 1
            mx = np.min([lx + 3, self.nx + 3])
            my = np.min([ly + 3, self.ny + 3])

            #We want to calculate the values of the functions from lx to mx and ly to my
            #We set it up like this to make it easier to see range
            for row in xrange(int(mx - lx + 1)):
                for column in xrange(int(my - ly + 1)):

                    #we take coeffs_row,column for each of the possible values in U_x X V_y
                    #And multiply it by the basis function value.  Same as in 1d, but in 2d

                    tempsum += self.c_mat[row + lx - 1, column + ly - 1] \
                    * self._calcuv_i(x, 'x', row + lx) * self._calcuv_i(y, 'y', column + ly)

            z_spline = tempsum

            return z_spline

        if type(point) == np.ndarray:
            point = np.atleast_2d(point)
            point = point if point.shape[0] == 2 else point.T
            z = np.zeros((point.shape[1], point.shape[1]))
            for ix in range(point.shape[1]):
                for iy in range(point.shape[1]):
                    x = point[0, ix]
                    y = point[1, iy]
                    z[ix, iy] = find_z_point(x, y)

        else:
            x = point[0]
            y = point[1]
            z = find_z_point(x, y)

        return z

    def gradient(self, point):
        """
        Evaluate the gradient of the spline at a given point.

        Parameters
        ----------
        point : array-like, dtype=float
            The point at which you want to evaluate the derivative.
            Right now this must be either a 1d np.ndarray or a single
            level list.

        Returns
        -------
        grad : array-like, dtype=float
            The gradient of the spline evaluated at the given point.
            This will be an np.ndarray of shape (n,), where n is the
            number of dimensions in the spline.

        Notes
        -----
        Uses numdifftools.Gradient.
        """
        g1 = lambda x : self.eval([x, point[1]])
        g2 = lambda r : self.eval([point[0], r])
        grad = np.zeros_like(point)
        grad[0] = der(g1, point[0], dx=0.0001, order=9)
        grad[1] = der(g2, point[1], dx=0.0001, order=9)
        return grad

    def grad2(self, point):
        g1 = lambda x : self.eval([x, point[1]])
        g2 = lambda r : self.eval([point[0], r])
        grad = np.zeros_like(point)
        ndg1 = nd.Derivative(g1)
        ndg2 = nd.Derivative(g2)
        grad[0] = ndg1(point[0])
        grad[1] = ndg2(point[1])
        return grad

#This needs to be renamed to comply with nosetest-ing.
'''
def test_1d():
    cs1d = CubicSpline1d(0, 4, 45)
    y = np.sin(cs1d.grid)
    cs1d.coefs(y)
    xtest = np.r_[1.0:4.0:5000j]
    ytest = np.zeros(xtest.size)
    for i in range(xtest.size):
        ytest[i] = cs1d.feval(xtest[i])
    err = np.sin(xtest) - ytest

    max_abs_err_1d = np.max(np.abs(err))
    mean_abs_err_1d = np.mean(np.abs(err))

    print '1d spline: Max absolute error is ', max_abs_err_1d
    print '1d spline: Mean absolute error is ', mean_abs_err_1d
'''

if __name__ == '__main__':
    # import mpl_toolkits.mplot3d.axes3d as p3
    # import matplotlib.pyplot as plt
    cs1d = CubicSpline1d(0, 4, 45)
    y = np.sin(cs1d.grid)
    cs1d.coefs(y)
    xtest = np.r_[1.0:4.0:5000j]
    ytest = np.zeros(xtest.size)
    for i in range(xtest.size):
        ytest[i] = cs1d.feval(xtest[i])
    err = np.sin(xtest) - ytest

    max_abs_err_1d = np.max(np.abs(err))
    mean_abs_err_1d = np.mean(np.abs(err))

    print '1d spline: Max absolute error is ', max_abs_err_1d
    print '1d spline: Mean absolute error is ', mean_abs_err_1d

    cs2d = CubicSpline2d(0, 0, 4, 4, 19, 19, 0, 0)
    Y, X = np.meshgrid(cs2d.ygrid, cs2d.xgrid)
    Z = np.sin(X) - np.cos(Y ** 2)
    cs2d.coefs(Z)
    xtest = np.r_[0.0:4.0:100j]
    ytest = np.r_[0.0:4.0:100j]
    ztest = cs2d.eval(np.row_stack([xtest, ytest]))

    # Build grid and get exact solution
    yy, xx = np.meshgrid(ytest, xtest)
    zz = np.sin(xx) - np.cos(yy ** 2)

    # Compute errors
    max_abs_err = np.abs(ztest - zz).max()
    mean_abs_err = np.abs(ztest - zz).mean()

    print '2d spline: Max absolute error is ', max_abs_err
    print '2d spline: Mean absolute error is ', mean_abs_err

    fun_real_grad = lambda x, y : np.array([np.cos(x), 2 * y * np.sin(y ** 2)])

    real_grad = fun_real_grad(0.22, 2.45)
    est_grad = cs2d.gradient([0.22, 2.45])
    grad_error = real_grad - est_grad
    print 'Error in gradient evaluation is: ', grad_error
