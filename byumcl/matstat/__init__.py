"""
matstat docstrings
"""

from byumcl.matstat.beta import Beta
from byumcl.matstat.binomial import Binomial
from byumcl.matstat.chi import Chi
from byumcl.matstat.chi_square import Chi_square
from byumcl.matstat.exponential import Exponential
from byumcl.matstat.f_dist import F_dist
from byumcl.matstat.gamma import Gamma
from byumcl.matstat.geometric import Geometric
from byumcl.matstat.inv_gamma import Inv_gamma
from byumcl.matstat.lognorm import Lognorm
from byumcl.matstat.normal import Normal
from byumcl.matstat.poisson import Poisson
from byumcl.matstat.student_t import Student_t
from byumcl.matstat.uniform import Uniform
